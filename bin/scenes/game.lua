-- [yue]: src/scenes/game.yue
local _module_0 = { } -- 1
local Scene -- 1
do -- 1
	local _obj_0 = require('../scene') -- 1
	Scene = _obj_0.Scene -- 1
end -- 1
local Room -- 2
do -- 2
	local _obj_0 = require('../room') -- 2
	Room = _obj_0.Room -- 2
end -- 2
local Globals -- 3
do -- 3
	local _obj_0 = require('../globals') -- 3
	Globals = _obj_0.Globals -- 3
end -- 3
local roomLayouts -- 4
do -- 4
	local _obj_0 = require('../roomLayouts') -- 4
	roomLayouts = _obj_0.roomLayouts -- 4
end -- 4
local createImageSpriteFunc, createTextSpriteFunc, createAnimatedMultiSpriteFunc, createTileSpriteFunc, BaseSprite -- 5
do -- 5
	local _obj_0 = require('../sprites') -- 5
	createImageSpriteFunc, createTextSpriteFunc, createAnimatedMultiSpriteFunc, createTileSpriteFunc, BaseSprite = _obj_0.createImageSpriteFunc, _obj_0.createTextSpriteFunc, _obj_0.createAnimatedMultiSpriteFunc, _obj_0.createTileSpriteFunc, _obj_0.BaseSprite -- 5
end -- 7
local dump -- 9
dump = function(o) -- 9
	if type(o) == 'function' then -- 10
		return 'function()' -- 11
	elseif type(o) == 'table' then -- 12
		local s = '{ ' -- 13
		for k, v in pairs(o) do -- 14
			if type(k) ~= 'number' then -- 15
				k = '"' .. k .. '"' -- 15
			end -- 15
			s = s .. ('[' .. k .. '] = ' .. dump(v) .. ',') -- 16
		end -- 16
		return s .. '} ' -- 17
	else -- 19
		return tostring(o) -- 19
	end -- 10
end -- 9
local _tileset -- 21
_tileset = function(x, y) -- 21
	return Res:loadQuadImage("wall_tiles.png", x, y, 32, 32) -- 21
end -- 21
local _tilesetRandom -- 22
_tilesetRandom = function(...) -- 22
	local choices = { -- 23
		... -- 23
	} -- 23
	local x, y -- 24
	do -- 24
		local _obj_0 = choices[math.random(#choices)] -- 24
		x, y = _obj_0[1], _obj_0[2] -- 24
	end -- 24
	return _tileset(x, y) -- 25
end -- 22
local _tilesetAnim -- 27
_tilesetAnim = function(...) -- 27
	local frames = { } -- 28
	local _list_0 = { -- 29
		... -- 29
	} -- 29
	for _index_0 = 1, #_list_0 do -- 29
		local _des_0 = _list_0[_index_0] -- 29
		local x, y = _des_0[1], _des_0[2] -- 29
		frames[#frames + 1] = _tileset(x, y) -- 30
	end -- 30
	return frames -- 31
end -- 27
local _tilesetSprite -- 33
_tilesetSprite = function(x, y) -- 33
	return createTileSpriteFunc(function() -- 33
		return _tileset(x, y) -- 33
	end) -- 33
end -- 33
local _tilesetRandomSprite -- 34
_tilesetRandomSprite = function(...) -- 34
	local vars = { -- 35
		... -- 35
	} -- 35
	return createTileSpriteFunc(function() -- 36
		return _tilesetRandom(unpack(vars)) -- 36
	end) -- 36
end -- 34
local _tilesetAnimSprite -- 38
_tilesetAnimSprite = function(...) -- 38
	local vars = { -- 39
		... -- 39
	} -- 39
	return createAnimatedMultiSpriteFunc((function() -- 40
		local _accum_0 = { } -- 40
		local _len_0 = 1 -- 40
		local _list_0 = _tilesetAnim(unpack(vars)) -- 40
		for _index_0 = 1, #_list_0 do -- 40
			local x = _list_0[_index_0] -- 40
			_accum_0[_len_0] = { -- 40
				createTileSpriteFunc(function() -- 40
					return x -- 40
				end) -- 40
			} -- 40
			_len_0 = _len_0 + 1 -- 40
		end -- 40
		return _accum_0 -- 40
	end)()) -- 40
end -- 38
local _roomMapping = { -- 43
	t = _tilesetRandomSprite({ -- 43
		16, -- 43
		0 -- 43
	}, { -- 43
		32, -- 43
		0 -- 43
	}), -- 43
	b = _tilesetSprite(16, 112), -- 44
	l = _tilesetSprite(0, 32), -- 45
	r = _tilesetSprite(128, 32), -- 46
	lt = _tilesetSprite(0, 0), -- 47
	rt = _tilesetSprite(128, 0), -- 48
	lb = _tilesetSprite(0, 112), -- 49
	rb = _tilesetSprite(128, 112), -- 50
	td = _tilesetAnimSprite({ -- 51
		64, -- 51
		0 -- 51
	}, { -- 51
		96, -- 51
		0 -- 51
	}), -- 51
	bd = _tilesetAnimSprite({ -- 52
		64, -- 52
		112 -- 52
	}), -- 52
	ld = _tilesetAnimSprite({ -- 53
		0, -- 53
		80 -- 53
	}), -- 53
	rd = _tilesetAnimSprite({ -- 54
		128, -- 54
		80 -- 54
	}), -- 54
	empty = function() -- 55
		return BaseSprite() -- 55
	end, -- 55
	help = createTextSpriteFunc(function() -- 56
		return Globals.text[Globals.lang].help -- 56
	end) -- 56
} -- 42
local _createRoom -- 58
_createRoom = function(layout, doors) -- 58
	return Room(layout, Globals.tileW, Globals.tileH, _roomMapping, doors) -- 58
end -- 58
local _createMap -- 60
_createMap = function(w, h) -- 60
	local layoutMap = { } -- 61
	for i = 1, h do -- 62
		layoutMap[i] = { } -- 62
	end -- 62
	local map = { } -- 63
	for i = 1, h do -- 64
		map[i] = { } -- 64
	end -- 64
	local createRooms -- 66
	createRooms = function(i, j, always, layout, first) -- 66
		if i == 0 or j == 0 or i == w + 1 or j == h + 1 then -- 67
			return false -- 69
		end -- 67
		if (layoutMap[i][j] ~= nil) then -- 70
			return false -- 72
		end -- 70
		if not always then -- 73
			local v = math.random() -- 74
			if v > Globals.roomProb then -- 75
				return false -- 77
			end -- 75
		end -- 73
		if layout ~= nil then -- 82
			layoutMap[i][j] = layout -- 82
		else -- 82
			layoutMap[i][j] = roomLayouts[math.random(#roomLayouts - 1) + 1] -- 82
		end -- 82
		local doors = { -- 85
			l = { -- 85
				i, -- 85
				j - 1 -- 85
			}, -- 85
			r = { -- 86
				i, -- 86
				j + 1 -- 86
			}, -- 86
			t = { -- 87
				i - 1, -- 87
				j -- 87
			}, -- 87
			b = { -- 88
				i + 1, -- 88
				j -- 88
			} -- 88
		} -- 84
		for _, _des_0 in pairs(doors) do -- 90
			i, j = _des_0[1], _des_0[2] -- 90
			createRooms(i, j, first) -- 90
		end -- 90
	end -- 66
	local createDoors -- 92
	createDoors = function(i, j) -- 92
		if i == 0 or j == 0 or i == w + 1 or j == h + 1 then -- 93
			return -- 93
		end -- 93
		if not (layoutMap[i][j] ~= nil) then -- 94
			return -- 94
		end -- 94
		if (map[i][j] ~= nil) then -- 95
			return -- 95
		end -- 95
		local doors = { -- 98
			l = { -- 98
				i, -- 98
				j - 1 -- 98
			}, -- 98
			r = { -- 99
				i, -- 99
				j + 1 -- 99
			}, -- 99
			t = { -- 100
				i - 1, -- 100
				j -- 100
			}, -- 100
			b = { -- 101
				i + 1, -- 101
				j -- 101
			} -- 101
		} -- 97
		map[i][j] = _createRoom(layoutMap[i][j], (function() -- 103
			local _tbl_0 = { } -- 103
			for k, _des_0 in pairs(doors) do -- 103
				local ii, jj = _des_0[1], _des_0[2] -- 103
				_tbl_0[k] = ((function() -- 103
					if ((function() -- 103
						local _exp_0 = layoutMap[ii] -- 103
						if _exp_0 ~= nil then -- 103
							return _exp_0 -- 103
						else -- 103
							return { } -- 103
						end -- 103
					end)())[jj] then -- 103
						return { -- 103
							ii, -- 103
							jj -- 103
						} -- 103
					else -- 103
						return nil -- 103
					end -- 103
				end)()) -- 103
			end -- 103
			return _tbl_0 -- 103
		end)()) -- 103
		for _, _des_0 in pairs(doors) do -- 104
			i, j = _des_0[1], _des_0[2] -- 104
			createDoors(i, j) -- 104
		end -- 104
		return true -- 106
	end -- 92
	createRooms(math.ceil(h / 2), math.ceil(w / 2), true, roomLayouts[1], true) -- 108
	createDoors(math.ceil(h / 2), math.ceil(w / 2)) -- 109
	return map, { -- 110
		math.ceil(h / 2), -- 110
		math.ceil(w / 2) -- 110
	} -- 110
end -- 60
local KeyInput -- 112
do -- 112
	local _class_0 -- 112
	local _base_0 = { -- 112
		update = function(self, dt) -- 118
			local isDown = love.keyboard.isDown(self.key) -- 119
			if self._pressed and not isDown then -- 120
				self:cb() -- 120
			end -- 120
			self._pressed = isDown -- 121
		end -- 112
	} -- 112
	if _base_0.__index == nil then -- 112
		_base_0.__index = _base_0 -- 112
	end -- 121
	_class_0 = setmetatable({ -- 112
		__init = function(self, key, cb) -- 113
			self.key = key -- 114
			self.cb = cb -- 115
			self._pressed = false -- 116
		end, -- 112
		__base = _base_0, -- 112
		__name = "KeyInput" -- 112
	}, { -- 112
		__index = _base_0, -- 112
		__call = function(cls, ...) -- 112
			local _self_0 = setmetatable({ }, _base_0) -- 112
			cls.__init(_self_0, ...) -- 112
			return _self_0 -- 112
		end -- 112
	}) -- 112
	_base_0.__class = _class_0 -- 112
	KeyInput = _class_0 -- 112
end -- 121
local GameScene -- 123
do -- 123
	local _class_0 -- 123
	local _parent_0 = Scene -- 123
	local _base_0 = { -- 123
		currentRoom = function(self) -- 142
			return self.map[self.coords[1]][self.coords[2]] -- 142
		end, -- 144
		update = function(self, dt) -- 144
			self:currentRoom():update(dt, self) -- 145
			local _list_0 = self.keys -- 146
			for _index_0 = 1, #_list_0 do -- 146
				local key = _list_0[_index_0] -- 146
				key:update(dt) -- 146
			end -- 146
		end, -- 148
		draw = function(self) -- 148
			self:currentRoom():draw(self) -- 149
			return Gfx:renderLayer("default", function() -- 150
				return { -- 151
					self:drawMap(200, 200, true), -- 151
					{ } -- 151
				} -- 151
			end) -- 150
		end, -- 153
		checkDoor = function(self) end, -- 155
		drawMap = function(self, dx, dy, hl) -- 155
			return function() -- 155
				for i = 1, self.h do -- 156
					for j = 1, self.w do -- 157
						local _continue_0 = false -- 158
						repeat -- 158
							local room = self.map[i][j] -- 158
							if not (room ~= nil) then -- 159
								_continue_0 = true -- 159
								break -- 159
							end -- 159
							if i == self.coords[1] and j == self.coords[2] then -- 160
								love.graphics.setColor(1, 1, 1) -- 161
							else -- 163
								love.graphics.setColor(0.5, 0.5, 0.5) -- 163
							end -- 160
							love.graphics.rectangle("fill", (j - 1) * 20, (i - 1) * 20, 20, 20) -- 164
							love.graphics.setColor(0.1, 0.1, 0.1) -- 165
							love.graphics.rectangle("line", (j - 1) * 20, (i - 1) * 20, 20, 20) -- 166
							love.graphics.setColor(1, 1, 1) -- 167
							_continue_0 = true -- 158
						until true -- 167
						if not _continue_0 then -- 167
							break -- 167
						end -- 167
					end -- 167
				end -- 167
			end -- 167
		end -- 123
	} -- 123
	if _base_0.__index == nil then -- 123
		_base_0.__index = _base_0 -- 123
	end -- 167
	setmetatable(_base_0, _parent_0.__base) -- 123
	_class_0 = setmetatable({ -- 123
		__init = function(self) -- 124
			self.w, self.h = 8, 8 -- 125
			self.map, self.coords = _createMap(self.w, self.h) -- 126
			self.pressedLastFrame = false -- 127
			self.keys = { -- 129
				KeyInput('up', function() -- 129
					if (self:currentRoom().doors.t ~= nil) then -- 130
						self.coords = self:currentRoom().doors.t -- 131
					end -- 130
				end), -- 129
				KeyInput('down', function() -- 132
					if (self:currentRoom().doors.b ~= nil) then -- 133
						self.coords = self:currentRoom().doors.b -- 134
					end -- 133
				end), -- 132
				KeyInput('left', function() -- 135
					if (self:currentRoom().doors.l ~= nil) then -- 136
						self.coords = self:currentRoom().doors.l -- 137
					end -- 136
				end), -- 135
				KeyInput('right', function() -- 138
					if (self:currentRoom().doors.r ~= nil) then -- 139
						self.coords = self:currentRoom().doors.r -- 140
					end -- 139
				end) -- 138
			} -- 128
		end, -- 123
		__base = _base_0, -- 123
		__name = "GameScene", -- 123
		__parent = _parent_0 -- 123
	}, { -- 123
		__index = function(cls, name) -- 123
			local val = rawget(_base_0, name) -- 123
			if val == nil then -- 123
				local parent = rawget(cls, "__parent") -- 123
				if parent then -- 123
					return parent[name] -- 123
				end -- 123
			else -- 123
				return val -- 123
			end -- 123
		end, -- 123
		__call = function(cls, ...) -- 123
			local _self_0 = setmetatable({ }, _base_0) -- 123
			cls.__init(_self_0, ...) -- 123
			return _self_0 -- 123
		end -- 123
	}) -- 123
	_base_0.__class = _class_0 -- 123
	if _parent_0.__inherited then -- 123
		_parent_0.__inherited(_parent_0, _class_0) -- 123
	end -- 123
	GameScene = _class_0 -- 123
end -- 167
_module_0["GameScene"] = GameScene -- 123
return _module_0 -- 167
