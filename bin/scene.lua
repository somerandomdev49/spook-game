-- [yue]: src/scene.yue
local _module_0 = { } -- 2
local Scene -- 2
do -- 2
	local _class_0 -- 2
	local _base_0 = { -- 2
		update = function(self, dt) end, -- 7
		draw = function(self) end -- 2
	} -- 2
	if _base_0.__index == nil then -- 2
		_base_0.__index = _base_0 -- 2
	end -- 7
	_class_0 = setmetatable({ -- 2
		__init = function(self, name) -- 3
			self.name = name -- 4
		end, -- 2
		__base = _base_0, -- 2
		__name = "Scene" -- 2
	}, { -- 2
		__index = _base_0, -- 2
		__call = function(cls, ...) -- 2
			local _self_0 = setmetatable({ }, _base_0) -- 2
			cls.__init(_self_0, ...) -- 2
			return _self_0 -- 2
		end -- 2
	}) -- 2
	_base_0.__class = _class_0 -- 2
	Scene = _class_0 -- 2
end -- 7
_module_0["Scene"] = Scene -- 2
return _module_0 -- 7
