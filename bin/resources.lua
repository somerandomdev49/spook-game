-- [yue]: src/resources.yue
local _module_0 = { } -- 1
local Image, QuadImage -- 1
do -- 1
	local _obj_0 = require('graphics') -- 1
	Image, QuadImage = _obj_0.Image, _obj_0.QuadImage -- 1
end -- 1
local Resources -- 3
do -- 3
	local _class_0 -- 3
	local _base_0 = { -- 3
		loadImage = function(self, name) -- 9
			if not (self.cache.images[name] ~= nil) then -- 10
				print("loadImage: " .. tostring(name) .. " not found, loading...") -- 11
				self.cache.images[name] = love.graphics.newImage("gfx/" .. name) -- 12
			end -- 10
			return Image(self.cache.images[name]) -- 13
		end, -- 15
		loadQuadImage = function(self, name, x, y, w, h) -- 15
			if not (self.cache.images[name] ~= nil) then -- 16
				print("loadQuadImage: " .. tostring(name) .. " not found, calling loadImage") -- 17
				self:loadImage(name) -- 18
			end -- 16
			return QuadImage(self.cache.images[name], love.graphics.newQuad(x, y, w, h, self.cache.images[name])) -- 20
		end -- 3
	} -- 3
	if _base_0.__index == nil then -- 3
		_base_0.__index = _base_0 -- 3
	end -- 20
	_class_0 = setmetatable({ -- 3
		__init = function(self) -- 4
			self.cache = { -- 6
				images = { }, -- 6
				sounds = { } -- 7
			} -- 5
		end, -- 3
		__base = _base_0, -- 3
		__name = "Resources" -- 3
	}, { -- 3
		__index = _base_0, -- 3
		__call = function(cls, ...) -- 3
			local _self_0 = setmetatable({ }, _base_0) -- 3
			cls.__init(_self_0, ...) -- 3
			return _self_0 -- 3
		end -- 3
	}) -- 3
	_base_0.__class = _class_0 -- 3
	Resources = _class_0 -- 3
end -- 20
_module_0["Resources"] = Resources -- 3
return _module_0 -- 20
