-- [yue]: src/graphics.yue
local _module_0 = { } -- 2
local Renderable -- 2
do -- 2
	local _class_0 -- 2
	local _base_0 = { -- 2
		render = function(self, x, y, r, sx, sy) -- 6
			return { -- 6
				function() end, -- 6
				{ } -- 6
			} -- 6
		end -- 2
	} -- 2
	if _base_0.__index == nil then -- 2
		_base_0.__index = _base_0 -- 2
	end -- 6
	_class_0 = setmetatable({ -- 2
		__init = function(self, width, height) -- 3
			self.width = width -- 4
			self.height = height -- 5
		end, -- 2
		__base = _base_0, -- 2
		__name = "Renderable" -- 2
	}, { -- 2
		__index = _base_0, -- 2
		__call = function(cls, ...) -- 2
			local _self_0 = setmetatable({ }, _base_0) -- 2
			cls.__init(_self_0, ...) -- 2
			return _self_0 -- 2
		end -- 2
	}) -- 2
	_base_0.__class = _class_0 -- 2
	Renderable = _class_0 -- 2
end -- 6
_module_0["Renderable"] = Renderable -- 2
local Image -- 8
do -- 8
	local _class_0 -- 8
	local _parent_0 = Renderable -- 8
	local _base_0 = { -- 8
		render = function(self, x, y, r, sx, sy) -- 12
			return { -- 12
				love.graphics.draw, -- 12
				{ -- 12
					self.image, -- 12
					x, -- 12
					y, -- 12
					r, -- 12
					sx, -- 12
					sy -- 12
				} -- 12
			} -- 12
		end -- 8
	} -- 8
	if _base_0.__index == nil then -- 8
		_base_0.__index = _base_0 -- 8
	end -- 12
	setmetatable(_base_0, _parent_0.__base) -- 8
	_class_0 = setmetatable({ -- 8
		__init = function(self, image) -- 9
			_class_0.__parent.__init(self, image:getWidth(), image:getHeight()) -- 10
			self.image = image -- 11
		end, -- 8
		__base = _base_0, -- 8
		__name = "Image", -- 8
		__parent = _parent_0 -- 8
	}, { -- 8
		__index = function(cls, name) -- 8
			local val = rawget(_base_0, name) -- 8
			if val == nil then -- 8
				local parent = rawget(cls, "__parent") -- 8
				if parent then -- 8
					return parent[name] -- 8
				end -- 8
			else -- 8
				return val -- 8
			end -- 8
		end, -- 8
		__call = function(cls, ...) -- 8
			local _self_0 = setmetatable({ }, _base_0) -- 8
			cls.__init(_self_0, ...) -- 8
			return _self_0 -- 8
		end -- 8
	}) -- 8
	_base_0.__class = _class_0 -- 8
	if _parent_0.__inherited then -- 8
		_parent_0.__inherited(_parent_0, _class_0) -- 8
	end -- 8
	Image = _class_0 -- 8
end -- 12
_module_0["Image"] = Image -- 8
local QuadImage -- 14
do -- 14
	local _class_0 -- 14
	local _parent_0 = Renderable -- 14
	local _base_0 = { -- 14
		render = function(self, x, y, r, sx, sy) -- 21
			return { -- 21
				love.graphics.draw, -- 21
				{ -- 21
					self.image, -- 21
					self.quad, -- 21
					x, -- 21
					y, -- 21
					r, -- 21
					sx, -- 21
					sy -- 21
				} -- 21
			} -- 21
		end -- 14
	} -- 14
	if _base_0.__index == nil then -- 14
		_base_0.__index = _base_0 -- 14
	end -- 21
	setmetatable(_base_0, _parent_0.__base) -- 14
	_class_0 = setmetatable({ -- 14
		__init = function(self, image, quad) -- 15
			local x, y, w, h = quad:getViewport() -- 16
			_class_0.__parent.__init(self, w, h) -- 17
			self.image = image -- 18
			self.quad = quad -- 19
		end, -- 14
		__base = _base_0, -- 14
		__name = "QuadImage", -- 14
		__parent = _parent_0 -- 14
	}, { -- 14
		__index = function(cls, name) -- 14
			local val = rawget(_base_0, name) -- 14
			if val == nil then -- 14
				local parent = rawget(cls, "__parent") -- 14
				if parent then -- 14
					return parent[name] -- 14
				end -- 14
			else -- 14
				return val -- 14
			end -- 14
		end, -- 14
		__call = function(cls, ...) -- 14
			local _self_0 = setmetatable({ }, _base_0) -- 14
			cls.__init(_self_0, ...) -- 14
			return _self_0 -- 14
		end -- 14
	}) -- 14
	_base_0.__class = _class_0 -- 14
	if _parent_0.__inherited then -- 14
		_parent_0.__inherited(_parent_0, _class_0) -- 14
	end -- 14
	QuadImage = _class_0 -- 14
end -- 21
_module_0["QuadImage"] = QuadImage -- 14
local Text -- 23
do -- 23
	local _class_0 -- 23
	local _parent_0 = Renderable -- 23
	local _base_0 = { -- 23
		render = function(self, x, y, r, sx, sy) -- 29
			return { -- 29
				love.graphics.print, -- 29
				{ -- 29
					self.text, -- 29
					x, -- 29
					y, -- 29
					r, -- 29
					sx, -- 29
					sy -- 29
				} -- 29
			} -- 29
		end -- 23
	} -- 23
	if _base_0.__index == nil then -- 23
		_base_0.__index = _base_0 -- 23
	end -- 29
	setmetatable(_base_0, _parent_0.__base) -- 23
	_class_0 = setmetatable({ -- 23
		__init = function(self, text, font) -- 24
			if font == nil then -- 25
				font = love.graphics.getFont() -- 25
			end -- 25
			_class_0.__parent.__init(self, font:getWidth(text), font:getHeight()) -- 26
			self.text = text -- 27
		end, -- 23
		__base = _base_0, -- 23
		__name = "Text", -- 23
		__parent = _parent_0 -- 23
	}, { -- 23
		__index = function(cls, name) -- 23
			local val = rawget(_base_0, name) -- 23
			if val == nil then -- 23
				local parent = rawget(cls, "__parent") -- 23
				if parent then -- 23
					return parent[name] -- 23
				end -- 23
			else -- 23
				return val -- 23
			end -- 23
		end, -- 23
		__call = function(cls, ...) -- 23
			local _self_0 = setmetatable({ }, _base_0) -- 23
			cls.__init(_self_0, ...) -- 23
			return _self_0 -- 23
		end -- 23
	}) -- 23
	_base_0.__class = _class_0 -- 23
	if _parent_0.__inherited then -- 23
		_parent_0.__inherited(_parent_0, _class_0) -- 23
	end -- 23
	Text = _class_0 -- 23
end -- 29
_module_0["Text"] = Text -- 23
local Graphics -- 31
do -- 31
	local _class_0 -- 31
	local _base_0 = { -- 31
		createLayer = function(self, name, index) -- 37
			self.layers[name] = { } -- 38
			if (index ~= nil) then -- 39
				self.layerOrder[index] = name -- 40
			else -- 42
				do -- 42
					local _obj_0 = self.layerOrder -- 42
					_obj_0[#_obj_0 + 1] = name -- 42
				end -- 42
			end -- 39
		end, -- 44
		translate = function(self, dx, dy) -- 44
			return self._transform:translate(dx, dy) -- 44
		end, -- 45
		rotate = function(self, a) -- 45
			return self._transform:rotate(a) -- 45
		end, -- 46
		scale = function(self, sx, sy) -- 46
			return self._transform:scale(sx, sy) -- 46
		end, -- 48
		renderLayer = function(self, layer, cmd) -- 48
			do -- 49
				local _obj_0 = self.layers[layer] -- 49
				_obj_0[#_obj_0 + 1] = { -- 49
					cmd(), -- 49
					self._transform:clone() -- 49
				} -- 49
			end -- 49
		end, -- 51
		draw = function(self) -- 51
			local _list_0 = self.layerOrder -- 51
			for _index_0 = 1, #_list_0 do -- 51
				local k = _list_0[_index_0] -- 51
				local _list_1 = self.layers[k] -- 52
				for _index_1 = 1, #_list_1 do -- 52
					local _des_0 = _list_1[_index_1] -- 52
					local cmd, transform = _des_0[1], _des_0[2] -- 52
					love.graphics.replaceTransform(transform) -- 53
					cmd[1](unpack(cmd[2])) -- 54
				end -- 54
				self.layers[k] = { } -- 55
			end -- 55
		end -- 31
	} -- 31
	if _base_0.__index == nil then -- 31
		_base_0.__index = _base_0 -- 31
	end -- 55
	_class_0 = setmetatable({ -- 31
		__init = function(self) -- 32
			self.layers = { } -- 33
			self.layerOrder = { } -- 34
			self._transform = love.math.newTransform() -- 35
		end, -- 31
		__base = _base_0, -- 31
		__name = "Graphics" -- 31
	}, { -- 31
		__index = _base_0, -- 31
		__call = function(cls, ...) -- 31
			local _self_0 = setmetatable({ }, _base_0) -- 31
			cls.__init(_self_0, ...) -- 31
			return _self_0 -- 31
		end -- 31
	}) -- 31
	_base_0.__class = _class_0 -- 31
	Graphics = _class_0 -- 31
end -- 55
_module_0["Graphics"] = Graphics -- 31
return _module_0 -- 55
