-- [yue]: src/globals.yue
local _module_0 = { } -- 2
local SCALE = 3 -- 2
_module_0["SCALE"] = SCALE -- 2
local Globals = { -- 4
	tileW = (32 * SCALE), -- 4
	tileH = (32 * SCALE), -- 5
	lang = "en", -- 6
	roomProb = 0.2, -- 7
	text = { -- 9
		en = { -- 10
			help = "" -- 10
		} -- 9
	} -- 8
} -- 3
_module_0["Globals"] = Globals -- 10
return _module_0 -- 10
