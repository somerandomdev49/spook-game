-- [yue]: src/room.yue
local _module_0 = { } -- 2
local _doorChecks -- 2
_doorChecks = { -- 3
	l = function(i, j, w, h) -- 3
		return i == math.ceil(h / 2) and j == 1 -- 3
	end, -- 3
	r = function(i, j, w, h) -- 4
		return i == math.ceil(h / 2) and j == w -- 4
	end, -- 4
	t = function(i, j, w, h) -- 5
		return i == 1 and j == math.ceil(w / 2) -- 5
	end, -- 5
	b = function(i, j, w, h) -- 6
		return i == h and j == math.ceil(w / 2) -- 6
	end -- 6
} -- 2
local _checkDoor -- 8
_checkDoor = function(i, j, width, height) -- 8
	for k, v in pairs(_doorChecks) do -- 9
		if v(i, j, width, height) then -- 10
			return k -- 10
		end -- 10
	end -- 10
	return nil -- 11
end -- 8
local Room -- 13
do -- 13
	local _class_0 -- 13
	local _base_0 = { -- 13
		update = function(self, dt, world) -- 32
			for i = 1, self.height do -- 33
				for j = 1, self.width do -- 34
					self.sprites[i][j]:update(dt, world) -- 35
					do -- 36
						local dir = _checkDoor(i, j, self.width, self.height) -- 36
						if dir then -- 36
							world:checkDoor(self.sprites[i][j], self.doors[dir]) -- 37
						end -- 36
					end -- 36
				end -- 38
			end -- 38
		end, -- 39
		draw = function(self, world) -- 39
			for i = 1, self.height do -- 40
				for j = 1, self.width do -- 41
					self.sprites[i][j]:draw(world) -- 42
				end -- 42
			end -- 42
		end -- 13
	} -- 13
	if _base_0.__index == nil then -- 13
		_base_0.__index = _base_0 -- 13
	end -- 42
	_class_0 = setmetatable({ -- 13
		__init = function(self, layout, w, h, map, doors) -- 14
			self.layout = layout -- 15
			self.height = #self.layout -- 16
			self.width = #(self.layout[1]) -- 17
			self.sprites = { } -- 18
			self.doors = doors -- 19
			for i = 1, self.height do -- 21
				self.sprites[i] = { } -- 22
				for j = 1, self.width do -- 23
					local dir = _checkDoor(i, j, self.width, self.height) -- 24
					if dir ~= nil and self.doors[dir] ~= nil then -- 25
						self.sprites[i][j] = (map[dir .. 'd'])((j - 1) * w, (i - 1) * h, w, h) -- 26
					else -- 29
						self.sprites[i][j] = (map[self.layout[i][j]])((j - 1) * w, (i - 1) * h, w, h) -- 29
					end -- 25
				end -- 31
			end -- 31
		end, -- 13
		__base = _base_0, -- 13
		__name = "Room" -- 13
	}, { -- 13
		__index = _base_0, -- 13
		__call = function(cls, ...) -- 13
			local _self_0 = setmetatable({ }, _base_0) -- 13
			cls.__init(_self_0, ...) -- 13
			return _self_0 -- 13
		end -- 13
	}) -- 13
	_base_0.__class = _class_0 -- 13
	Room = _class_0 -- 13
end -- 42
_module_0["Room"] = Room -- 13
return _module_0 -- 42
