-- [yue]: src/sprites.yue
local _module_0 = { } -- 1
local Text -- 1
do -- 1
	local _obj_0 = require("graphics") -- 1
	Text = _obj_0.Text -- 1
end -- 1
local BaseSprite -- 3
do -- 3
	local _class_0 -- 3
	local _base_0 = { -- 3
		update = function(self, dt) end, -- 10
		draw = function(self) end -- 3
	} -- 3
	if _base_0.__index == nil then -- 3
		_base_0.__index = _base_0 -- 3
	end -- 10
	_class_0 = setmetatable({ -- 3
		__init = function(self, x, y, w, h) -- 4
			self.x = x -- 5
			self.y = y -- 6
			self.w = w -- 7
			self.h = h -- 8
		end, -- 3
		__base = _base_0, -- 3
		__name = "BaseSprite" -- 3
	}, { -- 3
		__index = _base_0, -- 3
		__call = function(cls, ...) -- 3
			local _self_0 = setmetatable({ }, _base_0) -- 3
			cls.__init(_self_0, ...) -- 3
			return _self_0 -- 3
		end -- 3
	}) -- 3
	_base_0.__class = _class_0 -- 3
	BaseSprite = _class_0 -- 3
end -- 10
_module_0["BaseSprite"] = BaseSprite -- 3
local Sprite -- 12
do -- 12
	local _class_0 -- 12
	local _parent_0 = BaseSprite -- 12
	local _base_0 = { -- 12
		draw = function(self) -- 18
			return Gfx:renderLayer(self.layer, function() -- 19
				return self.renderable:render(self.x, self.y, 0, self.w / self.renderable.width, self.h / self.renderable.height) -- 20
			end) -- 19
		end -- 12
	} -- 12
	if _base_0.__index == nil then -- 12
		_base_0.__index = _base_0 -- 12
	end -- 20
	setmetatable(_base_0, _parent_0.__base) -- 12
	_class_0 = setmetatable({ -- 12
		__init = function(self, renderable, layer, x, y, w, h) -- 13
			_class_0.__parent.__init(self, x, y, w, h) -- 14
			self.renderable = renderable -- 15
			self.layer = layer -- 16
		end, -- 12
		__base = _base_0, -- 12
		__name = "Sprite", -- 12
		__parent = _parent_0 -- 12
	}, { -- 12
		__index = function(cls, name) -- 12
			local val = rawget(_base_0, name) -- 12
			if val == nil then -- 12
				local parent = rawget(cls, "__parent") -- 12
				if parent then -- 12
					return parent[name] -- 12
				end -- 12
			else -- 12
				return val -- 12
			end -- 12
		end, -- 12
		__call = function(cls, ...) -- 12
			local _self_0 = setmetatable({ }, _base_0) -- 12
			cls.__init(_self_0, ...) -- 12
			return _self_0 -- 12
		end -- 12
	}) -- 12
	_base_0.__class = _class_0 -- 12
	if _parent_0.__inherited then -- 12
		_parent_0.__inherited(_parent_0, _class_0) -- 12
	end -- 12
	Sprite = _class_0 -- 12
end -- 20
_module_0["Sprite"] = Sprite -- 12
local MultiSprite -- 22
do -- 22
	local _class_0 -- 22
	local _parent_0 = BaseSprite -- 22
	local _base_0 = { -- 22
		draw = function(self, world) -- 27
			Gfx:translate(self.x, self.y) -- 28
			local _list_0 = self.sprites -- 29
			for _index_0 = 1, #_list_0 do -- 29
				local sprite = _list_0[_index_0] -- 29
				sprite:draw(world) -- 29
			end -- 29
			return Gfx:translate(-self.x, -self.y) -- 30
		end, -- 32
		update = function(self, dt, world) -- 32
			local _list_0 = self.sprites -- 32
			for _index_0 = 1, #_list_0 do -- 32
				local sprite = _list_0[_index_0] -- 32
				sprite:update(dt, world) -- 32
			end -- 32
		end -- 22
	} -- 22
	if _base_0.__index == nil then -- 22
		_base_0.__index = _base_0 -- 22
	end -- 32
	setmetatable(_base_0, _parent_0.__base) -- 22
	_class_0 = setmetatable({ -- 22
		__init = function(self, sprites, x, y, w, h) -- 23
			_class_0.__parent.__init(self, x, y, w, h) -- 24
			self.sprites = sprites -- 25
		end, -- 22
		__base = _base_0, -- 22
		__name = "MultiSprite", -- 22
		__parent = _parent_0 -- 22
	}, { -- 22
		__index = function(cls, name) -- 22
			local val = rawget(_base_0, name) -- 22
			if val == nil then -- 22
				local parent = rawget(cls, "__parent") -- 22
				if parent then -- 22
					return parent[name] -- 22
				end -- 22
			else -- 22
				return val -- 22
			end -- 22
		end, -- 22
		__call = function(cls, ...) -- 22
			local _self_0 = setmetatable({ }, _base_0) -- 22
			cls.__init(_self_0, ...) -- 22
			return _self_0 -- 22
		end -- 22
	}) -- 22
	_base_0.__class = _class_0 -- 22
	if _parent_0.__inherited then -- 22
		_parent_0.__inherited(_parent_0, _class_0) -- 22
	end -- 22
	MultiSprite = _class_0 -- 22
end -- 32
_module_0["MultiSprite"] = MultiSprite -- 22
local AnimationType = { -- 35
	OnceAndStop = 1, -- 35
	OnceAndKill = 2, -- 36
	PingPong = 3, -- 37
	Repeat = 4 -- 38
} -- 34
_module_0["AnimationType"] = AnimationType -- 38
local AnimatedMultiSprite -- 40
do -- 40
	local _class_0 -- 40
	local _parent_0 = BaseSprite -- 40
	local _base_0 = { -- 40
		draw = function(self, world) -- 50
			Gfx:translate(self.x, self.y) -- 51
			do -- 52
				local _obj_0 = self.frames[self._current] -- 52
				if _obj_0 ~= nil then -- 52
					_obj_0:draw(world) -- 52
				end -- 52
			end -- 52
			return Gfx:translate(-self.x, -self.y) -- 53
		end, -- 55
		nextFrame = function(self) -- 55
			if self._current == 0 then -- 56
				return -- 56
			end -- 56
			if self.type == AnimationType.PingPong then -- 57
				if ((self._dir > 0 and self._current >= #self.frames) or (self._dir < 0 and self._current <= 1)) then -- 59
					self._dir = self._dir * (-1) -- 59
				end -- 59
			elseif self.type == AnimationType.OnceAndKill then -- 61
				if self._dir < 0 and self._current <= 1 then -- 63
					self._current = 0 -- 63
				end -- 63
				if self._dir > 0 and self._current >= #self.frames then -- 64
					self._current = 0 -- 64
				end -- 64
			elseif self.type == AnimationType.OnceAndStop then -- 65
				if self._dir < 0 and self._current <= 1 then -- 67
					self._dir = 0 -- 68
					self._current = 1 -- 69
				end -- 67
				if self._dir > 0 and self._current >= #self.frames then -- 70
					self._dir = 0 -- 71
					self._current = #self.frames -- 72
				end -- 70
			elseif self.type == AnimationType.Repeat then -- 73
				if self._dir < 0 and self._current <= 1 then -- 75
					self._current = #self.frames -- 76
				end -- 75
				if self._dir > 0 and self._current >= #self.frames then -- 77
					self._current = 1 -- 78
				end -- 77
			end -- 57
			self._current = self._current + self._dir -- 80
		end, -- 82
		update = function(self, dt, world) -- 82
			do -- 83
				local _obj_0 = self.frames[self._current] -- 83
				if _obj_0 ~= nil then -- 83
					_obj_0:update(dt, world) -- 83
				end -- 83
			end -- 83
			if self.opts.auto then -- 84
				if self._timer >= self.opts.frameTime then -- 85
					self._timer = 0 -- 86
					return self:nextFrame() -- 87
				else -- 89
					self._timer = self._timer + dt -- 89
				end -- 85
			end -- 84
		end -- 40
	} -- 40
	if _base_0.__index == nil then -- 40
		_base_0.__index = _base_0 -- 40
	end -- 89
	setmetatable(_base_0, _parent_0.__base) -- 40
	_class_0 = setmetatable({ -- 40
		__init = function(self, frames, type, opts, x, y, w, h) -- 41
			_class_0.__parent.__init(self, x, y, w, h) -- 42
			self.frames = frames -- 43
			self.type = type -- 44
			self.opts = opts -- 45
			self._timer = 0 -- 46
			do -- 47
				local _exp_0 = opts.start -- 47
				if _exp_0 ~= nil then -- 47
					self._current = _exp_0 -- 47
				else -- 47
					self._current = 1 -- 47
				end -- 47
			end -- 47
			do -- 48
				local _exp_0 = opts.direction -- 48
				if _exp_0 ~= nil then -- 48
					self._dir = _exp_0 -- 48
				else -- 48
					self._dir = 1 -- 48
				end -- 48
			end -- 48
		end, -- 40
		__base = _base_0, -- 40
		__name = "AnimatedMultiSprite", -- 40
		__parent = _parent_0 -- 40
	}, { -- 40
		__index = function(cls, name) -- 40
			local val = rawget(_base_0, name) -- 40
			if val == nil then -- 40
				local parent = rawget(cls, "__parent") -- 40
				if parent then -- 40
					return parent[name] -- 40
				end -- 40
			else -- 40
				return val -- 40
			end -- 40
		end, -- 40
		__call = function(cls, ...) -- 40
			local _self_0 = setmetatable({ }, _base_0) -- 40
			cls.__init(_self_0, ...) -- 40
			return _self_0 -- 40
		end -- 40
	}) -- 40
	_base_0.__class = _class_0 -- 40
	if _parent_0.__inherited then -- 40
		_parent_0.__inherited(_parent_0, _class_0) -- 40
	end -- 40
	AnimatedMultiSprite = _class_0 -- 40
end -- 89
_module_0["AnimatedMultiSprite"] = AnimatedMultiSprite -- 40
local createImageSpriteFunc -- 91
createImageSpriteFunc = function(image) -- 91
	return function(x, y, w, h) -- 92
		return Sprite(image(), "tile", x, y, w, h) -- 92
	end -- 92
end -- 91
_module_0["createImageSpriteFunc"] = createImageSpriteFunc -- 92
local createTileSpriteFunc -- 94
createTileSpriteFunc = function(image) -- 94
	return function(x, y, w, h) -- 95
		return Sprite(image(), "default", x, y, w, h) -- 95
	end -- 95
end -- 94
_module_0["createTileSpriteFunc"] = createTileSpriteFunc -- 95
local createTextSpriteFunc -- 97
createTextSpriteFunc = function(text) -- 97
	return function(x, y, w, h) -- 98
		return Sprite((Text(text())), "text", x, y, w, h) -- 98
	end -- 98
end -- 97
_module_0["createTextSpriteFunc"] = createTextSpriteFunc -- 98
local createMultiSpriteFunc -- 100
createMultiSpriteFunc = function(sprites) -- 100
	return function(x, y, w, h) -- 101
		return MultiSprite((function() -- 102
			local _accum_0 = { } -- 102
			local _len_0 = 1 -- 102
			for _index_0 = 1, #sprites do -- 102
				local _des_0 = sprites[_index_0] -- 102
				local sprite, xx, yy, ww, hh = _des_0[1], _des_0[2], _des_0[3], _des_0[4], _des_0[5] -- 102
				_accum_0[_len_0] = sprite((function() -- 102
					if xx ~= nil then -- 102
						return xx -- 102
					else -- 102
						return 0 -- 102
					end -- 102
				end)(), (function() -- 102
					if yy ~= nil then -- 102
						return yy -- 102
					else -- 102
						return 0 -- 102
					end -- 102
				end)(), (function() -- 102
					if ww ~= nil then -- 102
						return ww -- 102
					else -- 102
						return w -- 102
					end -- 102
				end)(), (function() -- 102
					if hh ~= nil then -- 102
						return hh -- 102
					else -- 102
						return h -- 102
					end -- 102
				end)()) -- 102
				_len_0 = _len_0 + 1 -- 102
			end -- 102
			return _accum_0 -- 102
		end)(), x, y, w, h) -- 103
	end -- 103
end -- 100
_module_0["createMultiSpriteFunc"] = createMultiSpriteFunc -- 103
local createAnimatedMultiSpriteFunc -- 105
createAnimatedMultiSpriteFunc = function(sprites, type, opts) -- 105
	return function(x, y, w, h) -- 106
		local frames -- 107
		do -- 107
			local _accum_0 = { } -- 107
			local _len_0 = 1 -- 107
			for _index_0 = 1, #sprites do -- 107
				local _des_0 = sprites[_index_0] -- 107
				local sprite, xx, yy, ww, hh = _des_0[1], _des_0[2], _des_0[3], _des_0[4], _des_0[5] -- 107
				_accum_0[_len_0] = sprite((function() -- 107
					if xx ~= nil then -- 107
						return xx -- 107
					else -- 107
						return 0 -- 107
					end -- 107
				end)(), (function() -- 107
					if yy ~= nil then -- 107
						return yy -- 107
					else -- 107
						return 0 -- 107
					end -- 107
				end)(), (function() -- 107
					if ww ~= nil then -- 107
						return ww -- 107
					else -- 107
						return w -- 107
					end -- 107
				end)(), (function() -- 107
					if hh ~= nil then -- 107
						return hh -- 107
					else -- 107
						return h -- 107
					end -- 107
				end)()) -- 107
				_len_0 = _len_0 + 1 -- 107
			end -- 107
			frames = _accum_0 -- 107
		end -- 107
		return AnimatedMultiSprite(frames, (function() -- 108
			if type ~= nil then -- 108
				return type -- 108
			else -- 108
				return AnimationType.Repeat -- 108
			end -- 108
		end)(), (function() -- 108
			if opts ~= nil then -- 108
				return opts -- 108
			else -- 108
				return { } -- 108
			end -- 108
		end)(), x, y, w, h) -- 108
	end -- 108
end -- 105
_module_0["createAnimatedMultiSpriteFunc"] = createAnimatedMultiSpriteFunc -- 108
return _module_0 -- 108
