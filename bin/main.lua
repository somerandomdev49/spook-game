-- [yue]: src/main.yue
local Globals -- 1
do -- 1
	local _obj_0 = require('globals') -- 1
	Globals = _obj_0.Globals -- 1
end -- 1
local Graphics -- 2
do -- 2
	local _obj_0 = require('graphics') -- 2
	Graphics = _obj_0.Graphics -- 2
end -- 2
local Resources -- 3
do -- 3
	local _obj_0 = require('resources') -- 3
	Resources = _obj_0.Resources -- 3
end -- 3
Gfx = Graphics() -- 5
Res = Resources() -- 6
love.graphics.setDefaultFilter('nearest', 'nearest') -- 8
love.window.setMode(Globals.tileW * 7, Globals.tileH * 7) -- 9
love.load = function() -- 11
	Gfx:createLayer("tile") -- 12
	Gfx:createLayer("default") -- 13
	Gfx:createLayer("text") -- 14
	Gfx:createLayer("gui") -- 15
	local GameScene = require('scenes/game').GameScene -- 16
	currentScene = GameScene() -- 17
end -- 11
love.draw = function() -- 19
	currentScene:draw() -- 20
	return Gfx:draw() -- 21
end -- 19
love.update = function(dt) -- 23
	return currentScene:update(dt) -- 23
end -- 23
